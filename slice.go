package slices

import (
	"github.com/pkg/errors"
)

func Batch[S ~[]E, E any](s S, batchSize int, f func(batch S) error) error {
	if batchSize <= 0 || len(s) == 0 {
		return nil
	}
	for start := 0; start < len(s); start += batchSize {
		end := min(start+batchSize, len(s))
		err := f(s[start:end])
		if err != nil {
			return errors.Wrapf(err, "batch processing %T from %d to %d", s, start, end)
		}
	}
	return nil
}

func Filter[S ~[]E, E any](s S, ok func(v E) bool) S {
	// Don't start copying elements until we find one to delete.
	for i, v := range s {
		if !ok(v) {
			j := i
			for i++; i < len(s); i++ {
				if v = s[i]; ok(v) {
					s[j], j = v, j+1
				}
			}
			return s[:j]
		}
	}
	return s
}

// Reorder sorts s in the order of keys. The values in keys must be unique, but
// the function is tolerant of non-unique values in s. Values from s not present
// in keys will be skipped.
func Reorder[S1 ~[]E1, S2 ~[]E2, E1 comparable, E2 any](keys S1, s S2, key func(obj E2) E1) S2 {
	if len(s) == 0 || len(keys) == 0 {
		return nil
	}

	m, n := make(map[E1]S2, len(s)), 0
	for i, v := range s {
		k := key(v)
		if tmp := m[k]; len(tmp) == 0 {
			m[k] = s[i : i+1 : i+1] // To avoid allocations for unique elements.
		} else {
			m[k], n = append(tmp, v), n+1
		}
	}

	s = make(S2, 0, len(keys)+n)
	for _, key := range keys {
		if v, ok := m[key]; ok {
			s = append(s, v...)
		}
	}
	return s
}

func Convert[S1 ~[]E1, E1, E2 any](a S1, f func(E1) E2) []E2 {
	result := make([]E2, len(a))
	for i, a := range a {
		result[i] = f(a)
	}
	return result
}

// Unique returns unique values from s. The function reuses s to store values, so
// s should not be used afterward. The order of the values is preserved.
func Unique[S ~[]E, E comparable](s S) S {
	if len(s) < 2 {
		return s
	}

	set, n, ok := make(map[E]struct{}, len(s)), 0, false
	for _, v := range s {
		if _, ok = set[v]; !ok {
			s[n], set[v], n = v, struct{}{}, n+1
		}
	}
	return s[:n]
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
